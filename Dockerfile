FROM nginx:1.15.8-alpine

copy ./nginx.conf /etc/nginx/nginx.conf

copy ./*.html /usr/share/nginx/html
copy ./*.png /usr/share/nginx/html
copy ./*.svg /usr/share/nginx/html
copy ./*.mp3 /usr/share/nginx/html
copy ./*.ico /usr/share/nginx/html

expose $PORT
